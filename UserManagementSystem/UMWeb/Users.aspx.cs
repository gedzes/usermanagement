﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMLibrary.BusinessObjects;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using UMLibrary;

namespace UMWeb
{
  public partial class Users1 : System.Web.UI.Page
  {

    public List<Group> groups { get; set; }
    public List<User> users { get; set; }
    public int selectedGroupID { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        GetGroups();
        populateDataGrid();
      }

    }
    private void populateDataGrid()
    {
      User user = new User();
      users = new List<User>();
      string conString = ConfigurationManager.ConnectionStrings["MainCon"].ConnectionString;

      using (SqlConnection con = new SqlConnection())
      {
        con.ConnectionString = conString;
        con.Open();
        try
        {
          using (SqlCommand sqlCmd = new SqlCommand("getUserList", con))
          {

            sqlCmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = sqlCmd.ExecuteReader();

            if (reader.HasRows)
            {

              while (reader.Read())
              {
                //            UserID, LoginName, EmailAddress, UserDescription, Password, 
                //ConfirmPassword, GroupID
                user.userID = reader.GetInt32(0);
                user.LogInName = reader.GetString(1);
                user.EmailAddress = reader.GetString(2);
                user.UserDescription = reader.GetString(3);
                user.GroupID = reader.GetValue(6);
                user.GroupName = reader.GetValue(7);
                user.Password = reader.GetString(4);
                user.ConfirmPassword = reader.GetString(5);
                users.Add(user);
                user = new User();
              }

              gvUsers.DataSource = users;
              gvUsers.DataBind();


              //DropDownList list = (DropDownList)e.Row.FindControl("ddlGroups"); ;

              //list.DataMember = "GroupID";
              //list.DataValueField = "GroupID";
              //list.DataTextField = "GroupName";
              //list.DataBind();
              //list.Items.Insert(0, new ListItem("--Select Group--", "0"));
              //list.DataSource = groups;
            }





          }
        }
        catch (Exception ex)
        {

          throw new Exception(ex.Message);
        }
        finally
        {
          con.Close();
        }


      }
    }
    private void GetGroups()
    {
      Group group = new Group();
      groups = new List<Group>();
      string conString = ConfigurationManager.ConnectionStrings["MainCon"].ConnectionString;

      using (SqlConnection con = new SqlConnection())
      {
        con.ConnectionString = conString;
        con.Open();
        try
        {
          using (SqlCommand sqlCmd = new SqlCommand("getGroupList", con))
          {

            sqlCmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = sqlCmd.ExecuteReader();

            if (reader.HasRows)
            {

              while (reader.Read())
              {

                group.GroupID = reader.GetInt32(0);
                group.GroupName = reader.GetString(1);
                group.GroupDescription = reader.GetString(2);
                group.Active = reader.GetBoolean(3);
                groups.Add(group);
                group = new Group();
              }



            }





          }
        }
        catch (Exception ex)
        {

          throw new Exception(ex.Message);
        }
        finally
        {
          con.Close();
        }


      }
    }


    protected void btnUpdate_Command(object sender, CommandEventArgs e)
    {






      Response.Redirect("CreateUser.aspx?UserID=" + e.CommandArgument );  





      string conString = ConfigurationManager.ConnectionStrings["MainCon"].ConnectionString;
   

    }
  }
}