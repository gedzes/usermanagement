﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateGroup.aspx.cs" Inherits="UMWeb.Create_Group" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
       <asp:HiddenField ID="hfUserID" runat="server" />
    <table>
      <tr>
        <td>
          <asp:Label ID="Label1" runat="server" Text="GroupName"></asp:Label>
        </td>
        <td>
          <asp:TextBox ID="txtGroupName" runat="server" text= '<%# group.GroupName %>'></asp:TextBox>
        </td>
      </tr>
      <tr>
        <td>
          <asp:Label ID="Label2" runat="server" Text="Group Description"></asp:Label>
        </td>
        <td>
          <asp:TextBox ID="txtGroupDescription" runat="server"  text= '<%# group.GroupDescription %>' TextMode="MultiLine"></asp:TextBox>
        </td>
      </tr>
      <tr>
        <td>
          <asp:Label ID="Label3" runat="server" Text="Active"></asp:Label>
        </td>
        <td>
          <asp:CheckBox ID="chkActive" runat="server" Checked = '<%# group.Active %>' />
        </td>
      </tr>
     
 
      <tr>
        <td>

        </td>
        <td colspan ="2">
          <asp:Button ID="btnSave" runat="server" Text="Save" CausesValidation ="true" OnCommand="btnSave_Command"   />
    
          <asp:Button ID="btnClear" runat="server" Text="Clear" OnCommand="btnClear_Command" />

        </td>
        <td>
          <asp:Label ID="lblSuccess" runat="server" Text="" ForeColor ="Green"></asp:Label>
        </td>
         <td>
          <asp:Label ID="lblError" runat="server" Text="" ForeColor ="Red"></asp:Label>
        </td>
      </tr>
    </table>
    <br />
  </div>
</asp:Content>
