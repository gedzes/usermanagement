﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="UMWeb.Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div>
     <asp:ValidationSummary ID="validationSummary" runat="server" ShowModelStateErrors="true" DisplayMode="List" ForeColor="Red"  /> 
  </div>
  <div>
       <asp:HiddenField ID="hfUserID" runat="server" />
    <table>
      <tr>
        <td>
          <asp:Label ID="Label1" runat="server" Text="LoginName"></asp:Label>
        </td>
        <td>
          <asp:TextBox ID="txtLoginName" runat="server" text= '<%# user.LogInName %>'></asp:TextBox>
        </td>
      </tr>
      <tr>
        <td>
          <asp:Label ID="Label2" runat="server" Text="EmailAddress"></asp:Label>
        </td>
        <td>
          <asp:TextBox ID="txtEmailAddress" runat="server"  text= '<%# user.EmailAddress %>' TextMode="Email"></asp:TextBox>
        </td>
      </tr>
      <tr>
        <td>
          <asp:Label ID="Label3" runat="server" Text="UserDescription"></asp:Label>
        </td>
        <td>
          <asp:TextBox ID="UserDescription" runat="server"  text= '<%# user.UserDescription %>' TextMode="MultiLine"></asp:TextBox>
        </td>
      </tr>
      <tr>
        <td>
          <asp:Label ID="Label4" runat="server" Text="Password" ></asp:Label>
        </td>
        <td>
          <asp:TextBox ID="Password" runat="server"  text=' <%# user.Password %>' TextMode="Password"></asp:TextBox>
        </td>
      </tr>
      <tr>
        <td>
          <asp:Label ID="Label5" runat="server" Text="ConfirmPassword"></asp:Label>
        </td>
        <td>
          <asp:TextBox ID="ConfirmPassword" runat="server"  text= ' <%# user.ConfirmPassword %>' TextMode="Password"></asp:TextBox>
        </td>
      </tr>
      <tr>
        <td>
          <asp:Label ID="Label6" runat="server" Text="Group"></asp:Label>
        </td>
        <td>
          <asp:TextBox ID="groupName" runat="server" Text ='<%# user.GroupName %>' Enabled ="false"></asp:TextBox>
        </td>
        
        <td></td>

      </tr>
      <tr>
        <td>

        </td>
        <td colspan ="2">
          <asp:Button ID="btnSave" runat="server" Text="Save" CausesValidation ="true" CommandName ="SaveUser" OnCommand="btnSave_Command" />
          
          <asp:Button ID="btnSave1" runat="server" Text="Update" CausesValidation ="true" CommandName ="SaveUser" OnCommand="btnSave1_Command" />
    
          <asp:Button ID="btnClear" runat="server" Text="Clear" CommandName ="Clear" OnCommand="btnClear_Command"/>

        </td>
         <td>
          <asp:Label ID="lblSuccess" runat="server" Text="" ForeColor ="Green"></asp:Label>
        </td>
      </tr>
    </table>
    <br />
  </div>
</asp:Content>
