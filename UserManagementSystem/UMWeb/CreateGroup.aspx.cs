﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMLibrary.BusinessObjects;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using UMLibrary;


namespace UMWeb
{
  public partial class Create_Group : System.Web.UI.Page
  {

    public  Group group { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {

      if (IsPostBack)
      {


        group = new Group();




        group.GroupName = txtGroupName.Text;
        group.GroupDescription = txtGroupDescription.Text;
        group.Active = chkActive.Checked;


        txtGroupName.DataBind();
        txtGroupDescription.DataBind();
        chkActive.DataBind();
   

      }
    }

    private void Clear()
    {
        txtGroupName.Text = "";
        txtGroupDescription.Text = "";
        chkActive.Checked = false;
    }

    private void SaveGroup()
    {
      string conString = ConfigurationManager.ConnectionStrings["MainCon"].ConnectionString;

      using (SqlConnection con = new SqlConnection())
      {
        con.ConnectionString = conString;
        con.Open();
        try
        {
          using (SqlCommand sqlCmd = new SqlCommand("insGroup", con))
          {

            sqlCmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramGroupID = sqlCmd.Parameters.AddWithValue("@GroupID", SqlDbType.Int);
            paramGroupID.Direction = ParameterDirection.Output;
            sqlCmd.Parameters.AddWithValue("@GroupName", Misc.NothingDBNull(group.GroupName));
            sqlCmd.Parameters.AddWithValue("@GroupDescription", Misc.NothingDBNull(group.GroupDescription));
            sqlCmd.Parameters.AddWithValue("@GroupActive", group.Active);
            
            sqlCmd.ExecuteNonQuery();
            Clear();
            lblSuccess.Text = "Saved Successfully";
          }






        }
        catch (Exception ex)
        {
          if (ex.Message.Contains("Cannot insert duplicate key row"))
          {
            lblError.Text = "The Group already exist in the Database. Statement was terminated.";
          }
          else
          {
            throw new Exception(ex.Message);
          }
        }
        finally
        {
          con.Close();
        }
      }


    }

    protected void btnSave_Command(object sender, CommandEventArgs e)
    {
      SaveGroup();
    }

    protected void btnClear_Command(object sender, CommandEventArgs e)
    {
      Clear();
    }
  }
}