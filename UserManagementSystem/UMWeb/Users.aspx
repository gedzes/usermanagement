﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="UMWeb.Users1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  
  <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns ="False" AllowPaging="true" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical"    >
    <AlternatingRowStyle BackColor="#CCCCCC" />
    <Columns>
        <%--LoginName,
  EmailAddress,
  UserDescription,
  Password,
  ConfirmPassword,
  GroupID--%>
      
      <asp:BoundField DataField="UserID" HeaderText="UserID" />
      <asp:BoundField DataField="LoginName" HeaderText="LoginName" />
      <asp:BoundField DataField="EmailAddress" HeaderText="Email Address" />
      <asp:BoundField DataField="UserDescription" HeaderText="User Description" />
      <asp:BoundField DataField="GroupName" HeaderText="GroupName" />
       <asp:TemplateField>
        <ItemTemplate>
          
         </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField>
        <ItemTemplate>
          <asp:Button ID="btnUpdate" runat="server" Text="Edit User" OnCommand ="btnUpdate_Command" UseSubmitBehavior ="false" CommandArgument='<%# Eval("UserID") %>' />
        </ItemTemplate>
      </asp:TemplateField>

    </Columns>
    <FooterStyle BackColor="#CCCCCC" />
    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
   
    <SortedAscendingCellStyle BackColor="#F1F1F1" />
    <SortedAscendingHeaderStyle BackColor="#808080" />
    <SortedDescendingCellStyle BackColor="#CAC9C9" />
    <SortedDescendingHeaderStyle BackColor="#383838" />
  </asp:GridView>
</asp:Content>
