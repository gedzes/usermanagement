﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMLibrary.BusinessObjects;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Data;
using UMLibrary;
using System.Text;
using System.Security.Cryptography; 



namespace UMWeb
{
  public partial class Users : System.Web.UI.Page
  {
    public User user { get; set; }

 
    public bool IsUpdate { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {


      if (!IsPostBack)
      {
        if (Request.QueryString["UserID"] != null)
        {
          btnSave.Enabled = false;


          user = new User();

          string conString = ConfigurationManager.ConnectionStrings["MainCon"].ConnectionString;
          using (SqlConnection con = new SqlConnection())
          {
            con.ConnectionString = conString;
            con.Open();
            try
            {
              using (SqlCommand sqlCmd = new SqlCommand("getUserList", con))
              {

                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(Request.QueryString["UserID"]));
                SqlDataReader reader = sqlCmd.ExecuteReader();

                if (reader.HasRows)
                {

                  while (reader.Read())
                  {

                    user.userID = reader.GetInt32(0);
                    user.LogInName = reader.GetString(1);
                    user.EmailAddress = reader.GetString(2);
                    user.UserDescription = reader.GetString(3);
                    user.GroupID = reader.GetValue(6);
                    user.GroupName = reader.GetValue(7);
                    

                  }
                  BindControls();



                }





              }
            }
            catch (Exception ex)
            {

              throw new Exception(ex.Message);
            }
            finally
            {
              con.Close();
            }


          }
        }
        else
        {



        }
      }
      else
      {
        user = new User();




        user.LogInName = txtLoginName.Text;
        user.EmailAddress = txtEmailAddress.Text;
        user.Password = Password.Text;
        user.ConfirmPassword = ConfirmPassword.Text;
        user.UserDescription = UserDescription.Text;



        BindControls();
      }

    
  
    }

    void ShowMessage(string msg)
    {
      ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('" + msg + "');</script>");
    }

    

   
    
    protected void btnClear_Click(object sender, EventArgs e)
    {
      clear();
    }

    public void clear()
    {
      txtEmailAddress.Text = "";
      txtLoginName.Text = "";
      ConfirmPassword.Text = "";
      Password.Text = "";
      UserDescription.Text = "";
     
    }

    private void BindControls () {
     txtEmailAddress.DataBind();
        txtLoginName.DataBind();
        ConfirmPassword.DataBind();
        Password.DataBind();
        UserDescription.DataBind();
        groupName.DataBind();
    }
    private void SaveUser()
    {
      string conString = ConfigurationManager.ConnectionStrings["MainCon"].ConnectionString;
      
      using (SqlConnection con = new SqlConnection())
      {
        con.ConnectionString = conString;
        con.Open();
        try
        {
          using (SqlCommand sqlCmd = new SqlCommand("insUser" , con))
          {
            
            sqlCmd.CommandType = CommandType.StoredProcedure;
            SqlParameter paramUserID = sqlCmd.Parameters.AddWithValue("@UserID", SqlDbType.Int);
            paramUserID.Direction = ParameterDirection.Output;
            sqlCmd.Parameters.AddWithValue("@LoginName", Misc.NothingDBNull(user.LogInName));
            sqlCmd.Parameters.AddWithValue("@EmailAddress", Misc.NothingDBNull(user.EmailAddress));
            sqlCmd.Parameters.AddWithValue("@UserDescription", Misc.NothingDBNull(user.UserDescription));
            sqlCmd.Parameters.AddWithValue("@Password", Misc.NothingDBNull(user.encryptedPassword));
            sqlCmd.Parameters.AddWithValue("@ConfirmPassword", Misc.NothingDBNull( user.ConfirmencryptedPassword));
            sqlCmd.Parameters.AddWithValue("@GroupID", Misc.NothingDBNull(user.GroupID));
            IsUpdate = false;
            sqlCmd.ExecuteNonQuery();
            clear();
            lblSuccess.Text = "Saved Successfully";
          }






        }
        catch (Exception ex)
        {

          throw new Exception(ex.Message);
        }
        finally
        {
          con.Close();
        }
      }


    }

     private void UpdateUser () {

       string conString = ConfigurationManager.ConnectionStrings["MainCon"].ConnectionString;

       using (SqlConnection con = new SqlConnection())
       {
         con.ConnectionString = conString;
         con.Open();
         try
         {
           using (SqlCommand sqlCmd = new SqlCommand("updUser", con))
           {

             sqlCmd.CommandType = CommandType.StoredProcedure;

             sqlCmd.Parameters.AddWithValue("@UserID", Convert.ToInt32(Request.QueryString["UserID"]));
             sqlCmd.Parameters.AddWithValue("@EmailAddress", Misc.NothingDBNull(user.EmailAddress));
             sqlCmd.Parameters.AddWithValue("@UserDescription", Misc.NothingDBNull(user.UserDescription));
           
            
             sqlCmd.ExecuteNonQuery();
             clear();
             lblSuccess.Text = "Updated Successfully";
           }






         }
         catch (Exception ex)
         {

           throw new Exception(ex.Message);
         }
         finally
         {
           con.Close();
         }
       }

     }

    public static string ComputeSha256Hash(string rawData)
    {
      // Create a SHA256   
      using (SHA256 sha256Hash = SHA256.Create())
      {
        // ComputeHash - returns byte array  
        byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

        // Convert byte array to a string   
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < bytes.Length; i++)
        {
          builder.Append(bytes[i].ToString("x2"));
        }
        return builder.ToString();
      }
    }


    protected void btnSave_Command(object sender, CommandEventArgs e)
    {
      SaveUser();
    }

    protected void btnClear_Command(object sender, CommandEventArgs e)
    {
      clear();
    }

    protected void btnSave1_Command(object sender, CommandEventArgs e)
    {
      UpdateUser();
      btnSave.Enabled = true;
    }
  }
}