﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

namespace UMWeb
{
  public partial class Report : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      this.ReportViewer6.ProcessingMode = ProcessingMode.Local;
      this.ReportViewer6.LocalReport.ReportPath = "UserManagementSystem/UMWeb/Report1.rdlc";
      ReportDataSource rds = new ReportDataSource("ReportDataSet");
      this.ReportViewer6.LocalReport.DataSources.Add(rds);
    }
  }
}