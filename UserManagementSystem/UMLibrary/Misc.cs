﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UMLibrary
{
  public class Misc
  {

    public static object ZeroDBNull(object Value)
    {
      if (Value.Equals(0))
        return DBNull.Value;
      else
        return Value;
    }

    public static object NothingDBNull(object value)
    {
      if (value == null)
        return DBNull.Value;
      else if (value is string && string.Empty.Equals(value))
        return DBNull.Value;
      else
        return value;
    }


  }
}
