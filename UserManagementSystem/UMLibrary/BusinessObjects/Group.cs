﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace UMLibrary.BusinessObjects
{
  public class Group
  {
    public int GroupID { get; set; }

    [Required(ErrorMessage = "Group required")]
    public string GroupName { get; set; }

    [StringLength(100, MinimumLength = 3,
     ErrorMessage = "Group Description Should be  3 characters and not more than 100 characters")]
    [DataType(DataType.Text)]
    public string GroupDescription { get; set; }

    public bool Active { get; set; }
  }
}
