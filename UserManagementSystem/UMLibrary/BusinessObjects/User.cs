﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography; 


namespace UMLibrary.BusinessObjects
{
    public class User
    {
      private string Password_Mask = "*****************";
      public string ConfirmencryptedPassword = "";
      public string encryptedPassword = "";
      private string password = "";
      private string confirmPassword = "";
      public int userID { get; set; }

     [Required(ErrorMessage="LogIn Name required")]
      public string LogInName { get; set; }

      [StringLength(100, MinimumLength = 3,
       ErrorMessage = "Description Should be  3 characters and not more than 100 characters")]
     [DataType(DataType.Text)]
      public string UserDescription { get; set; }

      [Required(ErrorMessage = "Password required")]
      [DataType(DataType.Password)]
      [PasswordPropertyText]
      public string Password
      {
        get
        {
          return password;
        }
        set
        {

          if (value != password)
          {
            
             password = ComputeSha256Hash(value);
             encryptedPassword = password;
           
          }
        }
      }
      [Required(ErrorMessage = "Confirm Password required")]
      [DataType(DataType.Password)]
      [PasswordPropertyText]
      public string ConfirmPassword
      {
        get
        {
          return confirmPassword;
        }
        set
        {

          if (value != confirmPassword)
          {

            confirmPassword = ComputeSha256Hash(value);
            ConfirmencryptedPassword = confirmPassword;
          }
        }
      }

      [Required(ErrorMessage = "Confirm Password required")]
      [DataType(DataType.EmailAddress)]
      [EmailAddress]
      public string EmailAddress { get; set; }

       [Required(ErrorMessage = "LogIn Name required")]
       public object GroupID { get; set; }



      public static string ComputeSha256Hash(string rawData)
       {
         // Create a SHA256   
         using (SHA256 sha256Hash = SHA256.Create())
         {
           // ComputeHash - returns byte array  
           byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

           // Convert byte array to a string   
           StringBuilder builder = new StringBuilder();
           for (int i = 0; i < bytes.Length; i++)
           {
             builder.Append(bytes[i].ToString("x2"));
           }
           return builder.ToString();
         }
       }

       public object GroupName { get; set; }
    }
}
